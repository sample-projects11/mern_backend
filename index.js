const express =require("express");
const app=express();
const dotenv=require("dotenv");
const cors = require("cors");
const mongoose  = require("mongoose");
const userRoute=require("./routes/user");
const authRoute=require("./routes/auth");
const ProductRoute=require("./routes/product");
const cartRoute=require("./routes/cart");
const orderRoute=require("./routes/order");
const stripeRoute=require("./routes/stripe");



dotenv.config();
//db
mongoose.connect(process.env.MONGO_URL
).then(()=>console.log("DB connection Successfull")).catch((err)=>{
  console.log(err);
});

  app.use(express.json())
  app.use(cors());
  app.use("/api/auth", authRoute);
  app.use("/api/users", userRoute);
  app.use("/api/products", ProductRoute);
  app.use("/api/carts", cartRoute);
  app.use("/api/orders", orderRoute);
  app.use("/api/checkout", stripeRoute);



app.listen(5000, ()=>{
    console.log("Backend Server is Running")
})